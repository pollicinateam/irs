package test.util;

import java.util.ArrayList;
import java.util.HashMap;

import pollicina.ir.similarity.TermStats;

public class CheckVectors {
	
	public static boolean equalStats(TermStats ts1, TermStats ts2) {
		if(ts1.isNumeric() != ts2.isNumeric()) {
			System.out.println("ts1 numeric: " + ts1.isNumeric() + " while ts2 numeric: " + ts2.isNumeric());
			return false;
		}
		if(ts1.isNumeric()) {
			if(ts1.getNumericValue() - ts2.getNumericValue() > Math.pow(10, -9)){
				System.out.println("different numeric values");
				return false;
			}
		}
		else {
			if(!ts1.getIdf().equals(ts2.getIdf())) {
				System.out.println("different idf");
				return false;
			}
			if(!ts1.getTf().equals(ts2.getTf())) {
				System.out.println("different tf");
				return false;
			}
		}
		return true;
	}

	public static boolean equal(HashMap<String, TermStats> v1, HashMap<String, TermStats> v2) {
		ArrayList<String> k1 = new ArrayList<String>(v1.keySet());
		ArrayList<String> k2 = new ArrayList<String>(v2.keySet());
		if(k1.size() != k2.size()) {
			System.out.println("different number of terms!");
			return false;
		}
		for(int i = 0; i < k1.size(); i++) {
			int j = 0;
			while(j < k2.size() && !k2.get(j).equals(k1.get(i))) {
				j++;
			}
			if(j >= k2.size()) {
				return false;
			}
			TermStats tv1 = v1.get(k1.get(i));
			TermStats tv2 = v2.get(k2.get(j));
			if(!equalStats(tv1, tv2)) {
				return false;
			}
		}
		return true;
	}
	
}
