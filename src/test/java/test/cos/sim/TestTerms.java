package test.cos.sim;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.junit.BeforeClass;
import org.junit.Test;

import pollicina.ir.datasetmanipulation.Normalizer;
import pollicina.ir.datasetmanipulation.NormalizerFactory;
import pollicina.ir.document.AnalyzerFactory;
import pollicina.ir.document.DocumentManager;
import pollicina.ir.document.DocumentManagerFactory;
import pollicina.ir.io.IndexManager;
import pollicina.ir.io.IndexManagerFactory;
import pollicina.ir.query.QueryManager;
import pollicina.ir.similarity.Stats;
import pollicina.ir.similarity.TermStats;

public class TestTerms {

	private static IndexManager indexUniManager;
	private static DocumentManager documentManager;
	private static Analyzer uniAnalyzer;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		indexUniManager = IndexManagerFactory.getBooksUniIndexManager();
		documentManager = DocumentManagerFactory.getBookManager();
		uniAnalyzer = AnalyzerFactory.createUniBookAnalyzer();
	}

	public void test() throws ParseException, IOException {
		QueryParser qp = new QueryParser("titolo", uniAnalyzer);
		ArrayList<Query> queries = new ArrayList<Query>();
		String queryRepeat = "fantozzi bambola";
		Query q = qp.parse("titolo:"+queryRepeat);
		System.out.println(q);
		Query q1 = qp.parse("descrizione:"+queryRepeat);
		Query q2 = qp.parse("autore:"+queryRepeat);
		queries.addAll(Arrays.asList(q, q1, q2));
		BooleanQuery.Builder bb = new BooleanQuery.Builder();
		for(int i = 0; i < queries.size(); i++) {
			bb.add(new BooleanClause(queries.get(i), BooleanClause.Occur.MUST));
		}
		Query finalQuery = bb.build();
		HashMap<String, TermStats> queryTerms = Stats.getQueryTerms(indexUniManager, finalQuery, 2543);
		String[] terms = {"bambol", "fantozz"};
		int i = 0;
		for(String term : queryTerms.keySet()) {
			System.out.println("term: " + term);
			assertTrue(term.equals(terms[i]));
			if(!queryTerms.get(term).isNumeric()) {
				System.out.println("tf: " + queryTerms.get(term).getTf());
				System.out.println("idf: " + queryTerms.get(term).getIdf());
			}
			else {
				System.out.println("numeric value: " + queryTerms.get(term).getNumericValue());
			}
			i++;
		}
	}

	@Test
	public void test2() throws ParseException, IOException {
		QueryParser qp = new QueryParser("titolo", uniAnalyzer);
		ArrayList<Query> queries = new ArrayList<Query>();
		String queryRepeat = "fantozzi bambola";
		Query q = qp.parse("titolo:"+queryRepeat);
		System.out.println(q);
		Query q1 = qp.parse("descrizione:"+queryRepeat);
		Query q2 = qp.parse("autore:"+queryRepeat);
		queries.addAll(Arrays.asList(q, q1, q2));
		BooleanQuery.Builder bb = new BooleanQuery.Builder();
		for(int i = 0; i < queries.size(); i++) {
			bb.add(new BooleanClause(queries.get(i), BooleanClause.Occur.MUST));
		}
		ArrayList<Document> documents = DocumentManager.createBooksDocuments();
		NormalizerFactory.createNormalizer("anno", documents);
		Query exactQ = QueryManager.getNormalizedExactQuery("anno", 1984);
		bb.add(new BooleanClause(exactQ, BooleanClause.Occur.MUST));
		HashMap<String, TermStats> queryTerms = Stats.getQueryTerms(indexUniManager, bb.build(), 2543);
		String[] terms = {"bambol", "0.9494949494949495", "fantozz"};
		int i = 0;
		for(String term : queryTerms.keySet()) {
			System.out.println("term: " + term);
			assertTrue(term.equals(terms[i]));
			if(!queryTerms.get(term).isNumeric()) {
				System.out.println("tf: " + queryTerms.get(term).getTf());
				System.out.println("idf: " + queryTerms.get(term).getIdf());
			}
			else {
				System.out.println("numeric value: " + queryTerms.get(term).getNumericValue());
			}
			i++;
		}
	}

}
