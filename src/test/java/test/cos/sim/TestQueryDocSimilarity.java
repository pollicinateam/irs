package test.cos.sim;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.junit.Test;

import pollicina.ir.datasetmanipulation.NormalizerFactory;
import pollicina.ir.document.AnalyzerFactory;
import pollicina.ir.document.DocumentManager;
import pollicina.ir.document.DocumentManagerFactory;
import pollicina.ir.io.IndexManager;
import pollicina.ir.io.IndexManagerFactory;
import pollicina.ir.query.QueryManager;
import pollicina.ir.similarity.Stats;
import pollicina.ir.similarity.TermStats;
import test.util.CheckVectors;

public class TestQueryDocSimilarity {

	private static final double EPSILON = Math.pow(10, -9);

	/**
	 * @throws ParseException
	 * @throws IOException
	 */
	@Test
	public void testSimilarities() throws ParseException, IOException {
		ArrayList<Document> docs = DocumentManager.createBooksDocuments();
		NormalizerFactory.createNormalizer("anno", docs);
		// IndexSearcher searcher =
		// IndexManagerFactory.getBooksUniIndexManager().createSearcher();
		IndexSearcher searcher1 = IndexManagerFactory.getBooksIndexManager().createSearcher();
		// System.out.println(searcher.getSimilarity(true));
		TopDocs td = QueryManager.execQuery(searcher1, "+titolo:romanzo +genere:romanzo",
				AnalyzerFactory.createBookAnalyzer(), 10);
		DocumentManagerFactory.getBookManager().printDocs(td, searcher1);
		td = QueryManager.execQuery(searcher1, "+titolo:\"La casa delle bambole\"",
				AnalyzerFactory.createBookAnalyzer(), 10);
		DocumentManagerFactory.getBookManager().printDocs(td, searcher1);
		IndexManager bookUniIndexMgr = IndexManagerFactory.getBooksUniIndexManager();
		IndexManager bookIndexMgr = IndexManagerFactory.getBooksIndexManager();
		Analyzer a = AnalyzerFactory.createUniBookAnalyzer();
		double sim4 = 0.0;
		double sim5 = 0.0;
		double sim6 = 0.0;
		double sim7 = 0.0;
		// //Il seguente � del 1974
		// sim4 = ComputeSimilarity.queryDocCosSim(bookUniIndexMgr,
		// QueryManager.andQueries(bookIndexMgr.createSearcher(), a, "titolo:fantozzi",
		// QueryManager.getNormalizedExactQuery("anno", 1984)), 2356,
		// NumericSimilarity.VECTOR);
		// System.out.println("similarity: " + sim4);
		// //165 Il seguente � del 1984
		// sim5 = ComputeSimilarity.queryDocCosSim(bookUniIndexMgr,
		// QueryManager.andQueries(bookIndexMgr.createSearcher(), a, "titolo:fantozzi",
		// QueryManager.getNormalizedExactQuery("anno", 1984)), 2357,
		// NumericSimilarity.VECTOR);
		// System.out.println("similarity: " + sim5);
		// //164 Il seguente � del 1994
		// sim6 = ComputeSimilarity.queryDocCosSim(bookUniIndexMgr,
		// QueryManager.andQueries(bookIndexMgr.createSearcher(), a, "titolo:fantozzi",
		// QueryManager.getNormalizedExactQuery("anno", 1984)), 2358,
		// NumericSimilarity.VECTOR);
		// System.out.println("similarity: " + sim6);
		// //Il seguente � del 2004
		// sim7 = ComputeSimilarity.queryDocCosSim(bookUniIndexMgr,
		// QueryManager.andQueries(bookIndexMgr.createSearcher(), a, "titolo:fantozzi",
		// QueryManager.getNormalizedExactQuery("anno", 1984)), 2359,
		// NumericSimilarity.VECTOR);
		// System.out.println("similarity: " + sim7);
		// assertTrue(sim4 < sim5 && sim6 < sim5 && (sim6 - sim4) < EPSILON && sim7 < sim5 && sim7 < sim6 && sim7 < sim4);
		String queryString = "titolo:\"La casa delle bambole\"";
		// Il seguente � del 1974
		sim4 = Stats.computeCosSim(bookUniIndexMgr, QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984)), 2356);
		System.out.println("similarity: " + sim4);
		// 165 Il seguente � del 1984
		sim5 = Stats.computeCosSim(bookUniIndexMgr, QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984)), 2357);
		System.out.println("similarity: " + sim5);
		// 164 Il seguente � del 1994
		sim6 = Stats.computeCosSim(bookUniIndexMgr, QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984)), 2359);
		System.out.println("similarity: " + sim6);
		// Il seguente � del 2004
		sim7 = Stats.computeCosSim(bookUniIndexMgr, QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984)), 2360);
		System.out.println("similarity: " + sim7);
		assertTrue(sim4 < sim5 && sim6 < sim5 && (sim6 - sim4) < EPSILON && sim7 < sim5 && sim7 < sim6 && sim7 < sim4);
		queryString = " +titolo:\"La casa delle bambole\" +autore:\"ka-tzetnik\" +casa:\"euroclub\" +genere:\"romanzo\" +lingua:\"italiano\" +note:\"disponibile\" ";
//		// Il seguente � del 1984
		sim4 = Stats.computeCosSim(bookUniIndexMgr, QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984)), 2357);
		Query q1 = QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984));
		// Il seguente � del 1984
		//Il seguente ha anno nullo
		sim5 = Stats.computeCosSim(bookUniIndexMgr, QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984)), 2358);
//		IndexReader reader = IndexManagerFactory.getBooksUniIndexManager().createReader();
//		System.out.println(reader.document(2358));
		System.out.println("similarity: " + sim4);
		System.out.println("similarity: " + sim5);
		assertTrue(sim5 < sim4);
		queryString = "La casa delle bambole ka-tzetnik euroclub romanzo italiano disponibile";
		Query q2 = QueryManager.andQueries(a, queryString, QueryManager.getNormalizedExactQuery("anno", 1984));
		sim6 = Stats.computeCosSim(bookUniIndexMgr, q2, 2357);
		assertEquals(sim4,sim6, Math.pow(10, -9));
		System.out.println("q1: " + q1);
		System.out.println("q2: " + q2);
		HashMap<String, TermStats> queryTermsQ1 = Stats.getQueryTerms(bookUniIndexMgr, q1, 2357);
		HashMap<String, TermStats> queryTermsQ2 = Stats.getQueryTerms(bookUniIndexMgr, q2, 2357);
//		assertTrue(q1.equals(q2));
		assertTrue(CheckVectors.equal(queryTermsQ1, queryTermsQ2));
	}

}
