package test.analyzer;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.it.ItalianLightStemmer;
import org.junit.BeforeClass;
import org.junit.Test;
import org.tartarus.snowball.ext.ItalianStemmer;

import pollicina.ir.document.AnalyzerFactory;

public class TestAnalyzer {
	
	private static Analyzer uniAnalyzer;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		uniAnalyzer = AnalyzerFactory.createUniArtworkAnalyzer();
	}

	@Test
	public void test() throws IOException {
//		Analyzer analzyer = AnalyzerFactory.createUniCustomAnalyzer();
//		ArrayList<Term> terms = ComputeSimilarity.getTerms(analzyer, "", "description");
//		for(int i = 0; i < terms.size(); i++) {
//			System.out.println(terms.get(i));
//		}
		ItalianLightStemmer stemmer = new ItalianLightStemmer();
		ItalianStemmer itStemmer = new ItalianStemmer();
		char[] word = "fasce".toCharArray();
		itStemmer.setCurrent(new String(word));
		int len = stemmer.stem(word, word.length);
		System.out.println(word);
		String wordString = new String(word);
		System.out.println("italian light stemmer: " + wordString.substring(0, len));
		itStemmer.stem();
		System.out.println("italian stemmer: " + itStemmer.getCurrent());
		assertEquals(itStemmer.getCurrent(), "fasc");
		word = "fascia".toCharArray();
		len = stemmer.stem(word, word.length);
		System.out.println(word);
		wordString = new String(word);
		itStemmer.setCurrent(wordString);
		System.out.println("italian light stemmer: " + wordString.substring(0, len));
		itStemmer.stem();
		System.out.println("italian stemmer: " + itStemmer.getCurrent());
		assertEquals(itStemmer.getCurrent(), "fasc");
		ArrayList<String> iniz = new ArrayList<String>(Arrays.asList("iniziare", "iniziando", "inizi�", "inizio", "iniziasse"));
		for(int i = 0; i < iniz.size(); i++) {
			itStemmer.setCurrent(iniz.get(i));
			itStemmer.stem();
			System.out.println(itStemmer.getCurrent());
			assertEquals(itStemmer.getCurrent(), "iniz");
		}
	}

}
