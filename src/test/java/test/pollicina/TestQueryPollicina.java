package test.pollicina;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.junit.BeforeClass;
import org.junit.Test;

import pollicina.ir.document.AnalyzerFactory;
import pollicina.ir.document.DocumentManager;
import pollicina.ir.document.DocumentManagerFactory;
import pollicina.ir.io.IndexManager;
import pollicina.ir.io.IndexManagerFactory;

public class TestQueryPollicina {
	
	private static Analyzer trigramAnalyzer;
	private static Analyzer unigramAnalyzer;
	private static IndexManager artworkManager;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		trigramAnalyzer = AnalyzerFactory.createArtworkAnalyzer();
		unigramAnalyzer = AnalyzerFactory.createUniArtworkAnalyzer();
		artworkManager = IndexManagerFactory.getPolliIndexManager();
	}
	
	private TopDocs genericQuery(String query) throws ParseException, IOException {
		QueryParser qp = new QueryParser("title", unigramAnalyzer);
		Query q = qp.parse(query);
		System.out.println("query parsed: " + q);
		IndexSearcher searcher = artworkManager.createSearcher();
		return searcher.search(q, 10);
	}

//	@Test
	public void test() throws ParseException, IOException {
		QueryParser qp = new QueryParser("nsc", unigramAnalyzer);
//		Query q = qp.parse("\"dio della morte\"");
		Query q = qp.parse("description:\"statua toro\"");
		System.out.println(q);
		System.out.println(q.getClass());
		IndexSearcher searcher = artworkManager.createSearcher();
		TopDocs td = searcher.search(q, 10);
		DocumentManager dm = DocumentManagerFactory.getArtworkManager();
		dm.printDocs(td, searcher);
	}
	
	@Test
	public void test2() throws ParseException, IOException {
		IndexSearcher searcher = artworkManager.createSearcher();
		TopDocs td = genericQuery("+description:iniziare");
		DocumentManager dm = DocumentManagerFactory.getArtworkManager();
		dm.printDocs(td, searcher);
	}
	
	@Test
	public void test3() throws IOException, ParseException {
		IndexSearcher searcher = artworkManager.createSearcher();
		TopDocs td = genericQuery("nsc:morte OR nsc:amore");
		DocumentManager dm = DocumentManagerFactory.getArtworkManager();
		dm.printDocs(td, searcher);
	}
	
	@Test
	public void test4() throws IOException, ParseException {
		IndexSearcher searcher = artworkManager.createSearcher();
		TopDocs td = genericQuery("vaso");
		DocumentManager dm = DocumentManagerFactory.getArtworkManager();
		dm.printDocs(td, searcher);
	}

}
