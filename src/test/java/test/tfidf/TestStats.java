package test.tfidf;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Test;

import pollicina.ir.io.IndexManager;
import pollicina.ir.io.IndexManagerFactory;
import pollicina.ir.similarity.Stats;
import pollicina.ir.similarity.TermStats;

public class TestStats {

	@Test
	public void testTFIDF() throws IOException {
		IndexManager manager = IndexManagerFactory.getBooksUniIndexManager();
		int docNum = 3520;
		HashMap<String, TermStats> tt = Stats.getTermTFIDF(manager.createReader(), docNum);
		for(String t : tt.keySet()) {
			if(t.equals("romanz")) {
				assertTrue(0.2f == tt.get(t).getTf());
			}
			System.out.println("term: " + t);
			System.out.println("tf: " + tt.get(t).getTf());
		}
	}
	
}
