package pollicina.ir.proofIndex;

import java.io.IOException;

import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

public class Explainator {
	
	public static void explain(Explanation[] explanations) {
//		System.out.println("qualcuno mi ha chiamato: YEEEEEEEEEEEEEE");
		if(explanations == null || explanations.length == 0) {
			System.out.println("no more explanations");
			return;
		}
		for(int i= 0; i < explanations.length; i++) {
//			System.out.println("waaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			explain(explanations[i]);
		}
	}
	
	public static void explain(Explanation explanation) {
		System.out.println("explanation: " + explanation.getDescription());
		explain(explanation.getDetails());
	}
	
	public static void explain(IndexSearcher searcher, Query q, int docInt) throws IOException {
		Explanation explanation = searcher.explain(q, docInt);
		explain(explanation);
	}

}
