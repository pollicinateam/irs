package pollicina.ir.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;

import pollicina.dao.ArtworksDao;
import pollicina.dto.ArtworkDto;
import pollicina.ir.document.AnalyzerFactory;
import pollicina.ir.document.DocumentManager;
import pollicina.ir.document.DocumentManagerFactory;
import pollicina.ir.dto.Artwork;
import pollicina.ir.io.IndexManager;
import pollicina.ir.io.IndexManagerFactory;
import pollicina.ir.mapper.ArtworkMapperImpl;

public class ArtworksService {

	public void indiciseArtworks(Connection conn) throws SQLException, IOException {
		ArtworksDao ad = new ArtworksDao();
//		ArrayList<Artwork> artworks = ad.getArtworks(conn);
//		ArrayList<Artwork> artworks = null;
		ArrayList<ArtworkDto> artworksDto = ad.getVerriArtworks(conn);
		ArrayList<Artwork> artworks = new ArrayList<Artwork>();
		ArtworkMapperImpl mapper = new ArtworkMapperImpl();
		for(int i = 0; i < artworksDto.size(); i++) {
			artworks.add(mapper.mapFromDto(artworksDto.get(i)));
		}
		DocumentManager dm = DocumentManagerFactory.getArtworkManager();
		ArrayList<Document> artDocs = dm.createArtworkDocuments(artworks);
		IndexManager indexUniManager = IndexManagerFactory.getPolliUniIndexManager();
		Analyzer artUniAnalyzer = AnalyzerFactory.createPollyUniCustomAnalyzer();
		indexUniManager.writeIndex(artDocs, artUniAnalyzer);
		IndexManager indexManager = IndexManagerFactory.getPolliIndexManager();
		Analyzer artAnalyzer = AnalyzerFactory.createPollyCustomAnalyzer();
//		Analyzer artAnalyzer = AnalyzerFactory.createCustomAnalyzer();
		indexManager.writeIndex(artDocs, artAnalyzer);
	}
	
}
