package pollicina.ir.service;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;

import pollicina.ir.datasetmanipulation.DatasetNormalizer;
import pollicina.ir.document.AnalyzerFactory;
import pollicina.ir.document.DocumentManager;
import pollicina.ir.io.IndexManagerFactory;

public class BooksService {
	
	public void indiciseBooks() throws IOException {
		ArrayList<Document> documents = DocumentManager.createBooksDocuments();
		Analyzer analyzer = AnalyzerFactory.createBookAnalyzer();
		DatasetNormalizer.normalizeNumericFields(documents);
		IndexManagerFactory.getBooksIndexManager().writeIndex(documents, analyzer);
		Analyzer unigramAnalyzer = AnalyzerFactory.createUniBookAnalyzer();
		DatasetNormalizer.normalizeNumericFields(documents);
		IndexManagerFactory.getBooksUniIndexManager().writeIndex(documents, unigramAnalyzer);
	}

}
