package pollicina.ir.dto;

public class Artwork {
	
	private String title;
	private String description;
	private String nsc;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNsc() {
		return nsc;
	}
	public void setNsc(String nsc) {
		this.nsc = nsc;
	}

}
