package pollicina.ir.query;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.util.BytesRef;

import pollicina.ir.datasetmanipulation.Normalizer;
import pollicina.ir.datasetmanipulation.NormalizerFactory;
import pollicina.ir.io.IndexManagerFactory;

public class QueryManager {
	
	private static final String DEFAULT_FIELD = "description";
	
	//� possibile eseguire il boosting passando una query con un apice "^" di fianco alla parola
	public static TopDocs execQuery(IndexSearcher searcher, String queryText, Analyzer analyzer, int maxDocs) throws ParseException, IOException {
		QueryParser parser = new QueryParser(DEFAULT_FIELD, analyzer);
		Query q = parser.parse(queryText);
		System.out.println(q);
		return searcher.search(q, maxDocs);
	}
	
	public static TopDocs queryStopeed(IndexSearcher searcher, Analyzer analyzer, int maxDocs) throws IOException{
		Term t1 = new Term("title", "the");
		PhraseQuery.Builder builder = new PhraseQuery.Builder();
		builder.add(t1);
		return searcher.search(builder.build(), maxDocs);
	}
	
	public static TopDocs phraseQueryBookTitle(Analyzer analyzer, int maxDocs, String word) throws IOException {
		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().createSearcher();
		Term term = new Term("titolo", "senza");
//		Term term1 = new Term("titolo", "collare");
		PhraseQuery.Builder pb = new PhraseQuery.Builder();
		pb.add(term);
//		pb.add(term1);
		return searcher.search(pb.build(), maxDocs);
	}
	
	public static TopDocs queryBookTitleTerm(Analyzer analyzer, int maxDocs, String word) throws IOException {
		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().createSearcher();
		Term t =new Term("titolo", word);
		TermQuery tq = new TermQuery(t);
		return searcher.search(tq, maxDocs);
	}
	
	public static TopDocs queryBookTitle(Analyzer analyzer, int maxDocs, String word) throws IOException, ParseException {
		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().createSearcher();
		QueryParser qp = new QueryParser("titolo", analyzer);
		Query query = qp.parse(word);
		return searcher.search(query, maxDocs);
		
//		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().getSearcher();
//		Term t1 = new Term("titolo", word);
//		TermQuery tq1 = new TermQuery(t1);
//		BooleanClause bc1 = new BooleanClause(tq1, BooleanClause.Occur.MUST);
//		BooleanQuery.Builder builder = new BooleanQuery.Builder();
//		builder.add(bc1);
//		return searcher.search(builder.build(), maxDocs);
	}
	
	public static Query andQueries(Analyzer a, String queryString, Query query) throws ParseException {
		QueryParser qp = new QueryParser("titolo", a);
		Query q = qp.parse(queryString);
		
		BooleanClause bc1 = new BooleanClause(q, BooleanClause.Occur.MUST);
		BooleanClause bc2 = new BooleanClause(query, BooleanClause.Occur.MUST);
		
		BooleanQuery.Builder builder = new BooleanQuery.Builder();
		builder.add(bc1);
		builder.add(bc2);
		
		return builder.build();
	}
	
	public static Query getNormalizedRangeQuery(String field, int r1, int r2) throws IOException {
		Normalizer normalizer = NormalizerFactory.getNormalizer(field);
		double rd1 = normalizer.normalize((double) r1);
		double rd2 = normalizer.normalize((double) r2);
		Query q = DoublePoint.newRangeQuery(field, rd1, rd2);
		System.out.println(q.toString());
		return q;
	}
	
	public static Query getRangeQuery(String field, int r1, int r2) throws IOException {
		Query q = IntPoint.newRangeQuery(field, r1, r2);
		System.out.println(q.toString());
		return q;
	}
	
	public static Query getNormalizedExactQuery(String field, int value) throws IOException {
		Normalizer normalizer = NormalizerFactory.getNormalizer(field);
		Double dValue = normalizer.normalize((double) value);
		Query q = DoublePoint.newExactQuery(field, dValue);
		
		System.out.println(q.toString());
		return q;
	}
	
	public static Query getExactQuery(String field, int value) throws IOException {
		Query q = IntPoint.newExactQuery(field, value);
		
		System.out.println(q.toString());
		return q;
	}
	
	public static TopDocs rangeQuery(IndexSearcher searcher, String field, int r1, int r2, int maxDocs) throws IOException {
		Query q = getNormalizedRangeQuery(field, r1, r2);
		return searcher.search(q, maxDocs);
	}
	
	public static TopDocs exactQuery(IndexSearcher searcher, String field, int value, int maxDocs) throws IOException {
		Query q = getNormalizedExactQuery(field, value);
		return searcher.search(q, maxDocs);
	}
	
	public static TopDocs queryLanguage(String language, int maxDocs) throws IOException {
		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().createSearcher();
		Term t = new Term("lingua", language);
		TermQuery tq = new TermQuery(t);
		BooleanClause bc1 = new BooleanClause(tq, BooleanClause.Occur.MUST);
		BooleanQuery.Builder builder = new BooleanQuery.Builder();
		builder.add(bc1);
		Query q = builder.build();
		System.out.println(q.toString());
		return searcher.search(q, maxDocs);
	}
	
	public static TopDocs customQuery(Analyzer a, IndexSearcher searcher, String query, int maxDocs) throws ParseException, IOException {
		QueryParser qp = new QueryParser("titolo", a);
		Query q = qp.parse(query);
		System.out.println(q.toString());
//		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().createSearcher();
		return searcher.search(q, maxDocs);
	}
	
	public static TopDocs rangeQueryYear(IndexSearcher searcher, String lowerYear, String upperYear, int maxDocs) throws IOException {
		BytesRef lower = new BytesRef(lowerYear);
		BytesRef upper = new BytesRef(upperYear);
		TermRangeQuery trq = new TermRangeQuery("anno", lower, upper, true, true);
//		Query q = IntPoint.newRangeQuery("anno", lowerYear, upperYear);
		System.out.println(trq.toString());
//		Weight w = searcher.createWeight(trq, true, 3.4f);
		return searcher.search(trq, maxDocs);
	}
	
	public static TopDocs intRangeQueryYear(IndexSearcher searcher, int lowerYear, int upperYear, int maxDocs) throws IOException {
//		TermRangeQuery trq = new TermRangeQuery("anno", lower, upper, true, true);
		Query trq = IntPoint.newRangeQuery("anno", lowerYear, upperYear);
//		Query q = IntPoint.newRangeQuery("anno", lowerYear, upperYear);
		System.out.println(trq.toString());
//		Weight w = searcher.createWeight(trq, true, 3.4f);
		return searcher.search(trq, maxDocs);
	}
	
	public static TopDocs queryLanguage(Analyzer a, String language, String notLanguage, int maxDocs) throws IOException, ParseException {
		IndexSearcher searcher = IndexManagerFactory.getBooksIndexManager().createSearcher();
		QueryParser qp = new QueryParser("lingua", a);
		Query q = qp.parse(language);
		BooleanClause bc1 = new BooleanClause(q, BooleanClause.Occur.MUST);
		BooleanQuery.Builder builder = new BooleanQuery.Builder();
		builder.add(bc1);
		Query query = builder.build();
		System.out.println(query.toString());
		return searcher.search(query, maxDocs);
	}
	
	public static TopDocs queryTitles(IndexSearcher searcher, Analyzer analyzer, int maxDocs) throws IOException {
		Term t1 = new Term("title", "bar");
		TermQuery tq1 = new TermQuery(t1);
		BooleanClause bc1 = new BooleanClause(tq1, BooleanClause.Occur.MUST);
		Term t2 = new Term("description", "new");
		TermQuery tq2 = new TermQuery(t2);
		BooleanClause bc2 = new BooleanClause(tq2, BooleanClause.Occur.MUST);
		BooleanQuery.Builder builder = new BooleanQuery.Builder();
		builder.add(bc1);
		builder.add(bc2);
		return searcher.search(builder.build(), maxDocs);
	}
	
	public static TopDocs proofBooleanQuery(IndexSearcher searcher, Analyzer analyzer, int maxDocs) throws IOException {
		Term t1 = new Term("description", "way");
		Term t2 = new Term("description", "student");
		Term t3 = new Term("description", "really");
		TermQuery tq = new TermQuery(t1);
		TermQuery tq1 = new TermQuery(t2);
		TermQuery tq2 = new TermQuery(t3);
//		BoostQuery qb1 = new BoostQuery(tq2, (float) 2.4);
		BooleanClause bc = new BooleanClause(tq, BooleanClause.Occur.MUST);
		BooleanClause bc1 = new BooleanClause(tq1, BooleanClause.Occur.MUST_NOT);
		BooleanClause bc2 = new BooleanClause(tq2, BooleanClause.Occur.MUST);
		BooleanQuery.Builder builder = new BooleanQuery.Builder();
		builder.add(bc);
		builder.add(bc1);
		builder.add(bc2);
		return searcher.search(builder.build(), maxDocs);
	}

}
