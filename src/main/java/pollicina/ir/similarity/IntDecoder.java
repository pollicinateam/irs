package pollicina.ir.similarity;

import org.apache.lucene.document.IntPoint;

public class IntDecoder implements Decoder {

	@Override
	public Double decode(byte[] bound, int offset) {
		return (double) IntPoint.decodeDimension(bound, offset);
	}

}
