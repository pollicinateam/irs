package pollicina.ir.similarity;

import java.util.HashMap;
import java.util.Map;

public class DecoderFactory {
	
	private static Map<String, Decoder> store;
	
	static {
		store = new HashMap<String, Decoder>();
		store.put("anno", new DoubleDecoder());
	}
	
	public static Decoder getDecoder(String fieldName) {
		if(store.containsKey(fieldName)) {
			return store.get(fieldName);
		}
		else {
			throw new IllegalArgumentException("Decoder for field \'" + fieldName + "\' has not been specified");
		}
	}

}
