package pollicina.ir.similarity;

import org.apache.lucene.document.DoublePoint;

public class DoubleDecoder implements Decoder {

	@Override
	public Double decode(byte[] bound, int offset) {
		return DoublePoint.decodeDimension(bound, offset);
	}

}
