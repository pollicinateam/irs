package pollicina.ir.similarity;

public interface Decoder {
	
	public Double decode(byte[] bound, int offset);

}
