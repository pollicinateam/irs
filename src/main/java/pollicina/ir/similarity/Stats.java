package pollicina.ir.similarity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PointRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Weight;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.BytesRef;

import pollicina.ir.io.IndexManager;

public class Stats {
	
	private static final double EPSILON = Math.pow(10, -9);
	
	private static Map<String, Float> idf;
	
	public static Map<String, Float> createIdf(IndexReader reader) throws IOException {
		if(idf == null) {
			idf = computeIdf(reader);
		}
		return idf;
	}
	
//	public static HashMap<String, TermStats> getTermStrTFIDF(IndexReader reader, int docNum) throws IOException {
//		HashMap<Term, TermStats> tt = getTermTFIDF(reader, docNum);
//		HashMap<String, TermStats> termStats = new HashMap<String, TermStats>();
//		for(Term term : tt.keySet()) {
//			
//		}
//	}
	
	public static double cosinSim(HashMap<String, TermStats> t1, HashMap<String, TermStats> query) {
		Set<String> tk1 = t1.keySet();
		Set<String> tk2 = query.keySet();
		double num = 0.0;
		for (String tt1 : tk1) {
			if (!t1.get(tt1).isNumeric() && query.containsKey(tt1)) {
				num += t1.get(tt1).weight() * query.get(tt1).weight();
			}
			//Questo va bene per entrambi, perch� 1 * sim(d,q) = sim(d,q)
			else if(t1.get(tt1).isNumeric()) {
				String fieldName = t1.get(tt1).getTerm().field();
				for (String tt2 : tk2) {
					if (query.get(tt2).getTerm().field().equals(fieldName)) {
						num += ComputeSimilarity.computeSimilarity(t1.get(tt1).getNumericValue(), query.get(tt2).getNumericValue());
						break;
					}
				}
			}
			else if (t1.get(tt1).isNumeric()) {
				String fieldName = t1.get(tt1).getTerm().field();
				for (String tt2 : tk2) {
					if (query.get(tt2).getTerm().field().equals(fieldName)) {
						num += t1.get(tt1).weight() * query.get(tt2).weight();
						break;
					}
				}
			}
		}
		double norm1 = 0.0;
		for (String term : t1.keySet()) {
			if(t1.get(term).isNumeric()) {
				norm1 += Math.pow(ComputeSimilarity.computeSimilarity(t1.get(term).getNumericValue(), t1.get(term).getNumericValue()), 2);
			}
			else {
				norm1 += Math.pow(t1.get(term).weight(), 2);
			}
		}
		norm1 = Math.sqrt(norm1);
		double norm2 = 0.0;
		for (String term : query.keySet()) {
			if (query.get(term).isNumeric()) {
				norm2 += Math.pow(ComputeSimilarity.computeSimilarity(query.get(term).getNumericValue(), query.get(term).getNumericValue()), 2);
			}
			else {
				norm2 += Math.pow(query.get(term).weight(), 2);
			}
		}
		norm2 = Math.sqrt(norm2);
		double den = norm1 * norm2;
		System.out.println("numerator: " + num);
		System.out.println("norm(t1): " + norm1);
		System.out.println("norm(query): " + norm2);
		if (num < EPSILON) {
			return 0.0;
		}
		return num / den;
	}
	
	public static double computeCosSim(IndexManager indexManager, Query q, int docId) throws IOException, ParseException {
		System.out.println("query to compare: " +q);
		HashMap<String, TermStats> doc = getTermTFIDF(indexManager.createReader(), docId);
		HashMap<String, TermStats> query = getQueryTerms(indexManager, q, docId);
		return cosinSim(doc, query);
	}

	
	/**
	 * 
	 * @param indexManager
	 * @param q
	 * @param docId
	 * @param sim
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public static HashMap<String, TermStats> getQueryTerms(IndexManager indexManager, Query q, int docId)
			throws ParseException, IOException {
		q = q.rewrite(indexManager.createReader());
		System.out.println("final query: " + q.toString());
		IndexSearcher searcher = indexManager.createSearcher();
		IndexReader reader = indexManager.createReader();
		Set<Term> termQuerySet = new HashSet<Term>();
//		Weight w = searcher.createWeight(q, true, 3.4f);
//		w.extractTerms(termQuerySet);
		System.out.println("term query set: " + termQuerySet);
		HashMap<String, TermStats> termsQuery = new HashMap<String, TermStats>();
		Map<String, Float> idf = Stats.createIdf(reader);
		ArrayList<PointRangeQuery> queryOnNumbers = new ArrayList<PointRangeQuery>();
		BooleanQuery bq = (BooleanQuery) q;
		int queryTermCount = 0;
		for (BooleanClause bc : bq.clauses()) {
			if (bc.getQuery() instanceof PointRangeQuery) {
				PointRangeQuery pointRange = (PointRangeQuery) bc.getQuery();
				String fieldName = pointRange.getField();
//				Document doc = reader.document(docId);
//				IndexableField[] fields = doc.getFields(fieldName);
//				System.out.println(doc.get("titolo"));
//				if (fields != null && fields.length > 0) {
//					System.out.println(NormalizerFactory.getNormalizer("anno")
//							.unNormalize(Double.parseDouble(reader.document(docId).get("anno"))));
//				}
				queryOnNumbers.add(pointRange);
				byte[] lowerp = pointRange.getLowerPoint();
				byte[] upperp = pointRange.getUpperPoint();
				Decoder decoder = DecoderFactory.getDecoder(fieldName);
				Double lowerPoint = decoder.decode(lowerp, 0);
				Double upperPoint = decoder.decode(upperp, 0);
				String docFieldValue = reader.document(docId).get(fieldName);
				Double numericValue = (upperPoint + lowerPoint) / 2.0;
				TermStats ts = new TermStats();
				ts.setNumeric(true);
				ts.setNumericValue(numericValue);
				Term t = new Term(fieldName, numericValue.toString());
				if (docFieldValue != null) {
					Double docFieldDoubleValue = Double.parseDouble(docFieldValue);
					if (docFieldDoubleValue <= upperPoint && docFieldDoubleValue >= lowerPoint) {
						numericValue = docFieldDoubleValue;
					}
					else if(docFieldDoubleValue - upperPoint < docFieldDoubleValue - lowerPoint) {
						numericValue = upperPoint;
					}
					else {
						numericValue = lowerPoint;
					}
					ts = new TermStats();
					ts.setNumeric(true);
					ts.setNumericValue(numericValue);
					t = new Term(fieldName, docFieldValue);
					ts.setTerm(t);
				}
				termsQuery.put(docFieldValue, ts);
				queryTermCount++;
			}
		}
		for (BooleanClause bc : bq.clauses()) {
			if(!(bc.getQuery() instanceof PointRangeQuery)) {
				Query repQuery = bc.getQuery();
				Weight w = searcher.createWeight(repQuery, true, 3.4f);
				w.extractTerms(termQuerySet);
				for (Term term : termQuerySet) {
//					if (reader.document(0).getField(term.field()).numericValue() == null && idf.containsKey(term.text())) {
					if (idf.containsKey(term.text())) {
						float freqTerm = 0;
						for (Term term1 : termQuerySet) {
							if (term.equals(term1)) {
								freqTerm++;
							}
						}
						freqTerm = freqTerm / (float) (termQuerySet.size() + queryTermCount);
						TermStats ts = new TermStats();
						ts.setTerm(term);
						ts.setTf(freqTerm);
						ts.setIdf(idf.get(term.text()));
						termsQuery.put(term.text(), ts);
					} else {
						System.out.println("il termine " + term.text() + " della query non � mai presente nei documenti");
					}
				}
				break;
			}
		}
		// Weight w = new ConstantScoreWeight(q, 3.4f);
		return termsQuery;
	}
	
	public static HashMap<String, TermStats> getTermTFIDF(IndexReader reader, int docNum) throws IOException {
		Document doc = reader.document(docNum);
		HashMap<String, TermStats> termWeight = new HashMap<String, TermStats>();
		HashMap<String, ArrayList<String>> mapTermFields = new HashMap<String, ArrayList<String>>();
		int termDocCount = 0;
		Map<String, Float> idfTerms = Stats.createIdf(reader);
		for (IndexableField field : doc.getFields()) {
			String fieldName = field.name();
			if (field.fieldType().storeTermVectors()) {
				Terms termVector = reader.getTermVector(docNum, fieldName);
				TermsEnum itr = termVector.iterator();
				BytesRef term = null;
				PostingsEnum postings = null;
				while ((term = itr.next()) != null) {
					String termName = term.utf8ToString();
					postings = itr.postings(postings, PostingsEnum.FREQS);
					postings.nextDoc();
					float tf = (float) postings.freq();
//					System.out.println("term: " + term.utf8ToString());
//					System.out.println("tf: " + tf);
//					if(term.utf8ToString().equals("romanz")) {
//						System.out.println("computeSimilarity:");
//						System.out.println("fieldName: " + field.name());
//					}
					float idf = idfTerms.get(term.utf8ToString());
					if(!mapTermFields.containsKey(termName)) {
						TermStats ts = new TermStats();
						ts.setTf(tf);
						ts.setIdf(idf);
						ts.setTerm(new Term(fieldName, termName));
						termWeight.put(termName, ts);
						mapTermFields.put(termName, new ArrayList<String>(Arrays.asList(fieldName)));
					}
					else if(!mapTermFields.get(termName).contains(fieldName)) {
						float newTf = termWeight.get(termName).getTf() + tf;
						termWeight.get(termName).setTf(newTf);
						mapTermFields.get(termName).add(fieldName);
					}
					termDocCount++;
				}
			} else {
				String value = field.stringValue();
				Term t = new Term(field.name(), value);
				System.out.println(field.name() + ": " + field.numericValue());
				if (field.numericValue() != null) {
					double fieldValue = Double.parseDouble(value);
					TermStats ts = new TermStats();
					ts.setNumeric(true);
					ts.setNumericValue(fieldValue);
					ts.setTerm(t);
					termWeight.put(t.text(), ts);
				} else {
					System.out.println("Non sono stati salvati i termini del campo \'" + field.name()
							+ "\'. Tutti i termini dei campi devono essere salvati.");
					System.exit(-1);
				}
				termDocCount++;
			}
		}
		for (String t : termWeight.keySet()) {
			if (!termWeight.get(t).isNumeric()) {
				float tf = termWeight.get(t).getTf();
				termWeight.get(t).setTf(tf / (float) termDocCount);
			}
		}
		return termWeight;
	}
	
	/**
	 * This methods
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	private static Map<String, Float> computeIdf(IndexReader reader) throws IOException {
		
		HashMap<String, ArrayList<String>> termFields = new HashMap<String, ArrayList<String>>();
		
		Map<String, Long> docFrequencies = new HashMap<>();
		HashMap<String, Float> idfTerms = new HashMap<String, Float>();
		//		 The function below is field-specific and the value is calculated while looping through the termsEnum:

		/*** GET ALL THE IDFs ***/
		/** GET FIELDS **/
		Fields fields = MultiFields.getFields(reader); //Get the Fields of the index 

		TFIDFSimilarity tfidfSIM = new ClassicSimilarity();

		for (String f: fields) 
		{
			TermsEnum termEnum = MultiFields.getTerms(reader, f).iterator();
			BytesRef bytesRef;
			while ((bytesRef = termEnum.next()) != null) 
			{
				if (termEnum.seekExact(bytesRef)) 
				{
					String term = bytesRef.utf8ToString();
					long ni = termEnum.docFreq();
					long docNum = reader.numDocs();
//					float idf = tfidfSIM.idf( ni, docNum );
					if(!termFields.containsKey(term)) {
//						idfTerms.put(term, docNum);
						docFrequencies.put(term, ni);
						termFields.put(term, new ArrayList<String>(Arrays.asList(f)));
					}
					else if(!termFields.get(term).contains(f)) {
						long newValue = docFrequencies.get(term) + ni;
						docFrequencies.put(term, newValue);
						termFields.get(term).add(f);
					}
				}
			}
		}

		for(String term : docFrequencies.keySet()) {
			float idf = tfidfSIM.idf(docFrequencies.get(term), reader.numDocs() );
			if(term.equals("romanz")) {
				System.out.println("doc freq: " + docFrequencies.get(term));
				System.out.println("idf: " + idf);
			}
			idfTerms.put(term, idf);
		}
		
		return idfTerms;
	}

}
