package pollicina.ir.similarity;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PointRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.Weight;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.BytesRef;

import pollicina.ir.io.IndexManager;
import pollicina.ir.io.IndexManagerFactory;

public class ComputeSimilarity {

	private static final double EPSILON = Math.pow(10, -9);
	
	public enum NumericSimilarity {COMPUTE, VECTOR};

//	public static void printBoh(Analyzer analyzer) throws IOException {
//		String textField = "title";
//		TokenStream ts = analyzer.tokenStream(textField,
//				"Raise the Bar: An Action-Based Method for Maximum Customer Reactions [Kindle Edition]");
//		// The Analyzer class will construct the Tokenizer, TokenFilter(s), and
//		// CharFilter(s),
//		// and pass the resulting Reader to the Tokenizer.
//		// OffsetAttribute offsetAtt = ts.addAttribute(OffsetAttribute.class);
//		CharTermAttribute charTermAttribute = ts.addAttribute(CharTermAttribute.class);
//		BooleanQuery.Builder builder = null;
//		try {
//			ts.reset(); // Resets this stream to the beginning. (Required)
//			builder = new BooleanQuery.Builder();
//			while (ts.incrementToken()) {
//				// Use AttributeSource.reflectAsString(boolean)
//				// for token stream debugging.
//				String tokenTerm = ts.reflectAsString(true);
//				System.out.println("token: " + tokenTerm);
//				String textTerm = charTermAttribute.toString();
//				System.out.println(textTerm);
//				Term term = new Term(textField, textTerm);
//				TermQuery tq = new TermQuery(term);
//				BooleanClause clause = new BooleanClause(tq, BooleanClause.Occur.SHOULD);
//				builder.add(clause);
//			}
//			ts.end(); // Perform end-of-stream operations, e.g. set the final offset.
//		} finally {
//			ts.close(); // Release resources associated with this stream.
//		}
//		IndexSearcher searcher = IndexManagerFactory.getMovieIndexManager().createSearcher();
//		// searcher.setSimilarity(new BM25Similarity());
//		TopDocs docs = searcher.search(builder.build(), 1000);
//		System.out.println("total hits: " + docs.totalHits);
//		// DocumentManager.printDocs(docs);
//	}
	
	public static ArrayList<Term> getTerms(Analyzer analyzer, String doc, String fieldName) throws IOException {
		TokenStream ts = analyzer.tokenStream(fieldName, doc);
		// The Analyzer class will construct the Tokenizer, TokenFilter(s), and
		// CharFilter(s),
		// and pass the resulting Reader to the Tokenizer.
		// OffsetAttribute offsetAtt = ts.addAttribute(OffsetAttribute.class);
		CharTermAttribute charTermAttribute = ts.addAttribute(CharTermAttribute.class);
//		BooleanQuery.Builder builder = null;
		ArrayList<Term> terms = new ArrayList<Term>();
		try {
			ts.reset(); // Resets this stream to the beginning. (Required)
//			builder = new BooleanQuery.Builder();
			while (ts.incrementToken()) {
				// Use AttributeSource.reflectAsString(boolean)
				// for token stream debugging.
//				String tokenTerm = ts.reflectAsString(true);
//				System.out.println("token: " + tokenTerm);
				String textTerm = charTermAttribute.toString();
				System.out.println("token: " + textTerm);
				BytesRef normToken = analyzer.normalize(fieldName, textTerm);
				String normalizedToken = normToken.utf8ToString();
				Term term = new Term(fieldName, normalizedToken);
				System.out.println("normalized token: " + normalizedToken);
				TermQuery tq = new TermQuery(term);
				terms.add(term);
//				BooleanClause clause = new BooleanClause(tq, BooleanClause.Occur.SHOULD);
//				builder.add(clause);
			}
			ts.end(); // Perform end-of-stream operations, e.g. set the final offset.
		} finally {
			ts.close(); // Release resources associated with this stream.
		}
		return terms;
	}

	@Deprecated
	public static HashMap<Term, TermStats> getTermTFIDF(IndexReader reader, int docNum) throws IOException {
		// int docNum = 8;
		// TFIDFSimilarity tfidf = new TFIDFSimilarity();
		Long numDocs = (long) reader.numDocs();
		Document doc = reader.document(docNum);
		HashMap<Term, TermStats> termWeight = new HashMap<Term, TermStats>();
		TFIDFSimilarity tfidfSIM = new ClassicSimilarity();
		int termDocCount = 0;
		Map<String, Float> idfTerms = Stats.createIdf(reader);
		for (IndexableField field : doc.getFields()) {
			String fieldName = field.name();
			// System.out.println(fieldName);
			if (field.fieldType().storeTermVectors()) {
				Terms termVector = reader.getTermVector(docNum, fieldName);
				// Fields termVector1 = reader.getTermVectors(docNum);
				TermsEnum itr = termVector.iterator();
				BytesRef term = null;
				PostingsEnum postings = null;
				while ((term = itr.next()) != null) {
					Term currentTerm = new Term(fieldName, term);
					// String termText = term.utf8ToString();
					postings = itr.postings(postings, PostingsEnum.FREQS);
					// while(postings.nextDoc() != PostingsEnum.NO_MORE_DOCS) {
					// System.out.println(postings.freq());
					// }
					postings.nextDoc();
					float tf = (float) postings.freq();
					// double docFreq = itr.docFreq();
					Long docFreq = (long) reader.docFreq(currentTerm);
					if(term.utf8ToString().equals("romanz")) {
						System.out.println("computeSimilarity:");
						System.out.println("fieldName: " + field.name());
						System.out.println("docFreq: " + docFreq);
					}
					// System.out.println(docFreq);
					float idf = tfidfSIM.idf(docFreq, numDocs);
					idf = idfTerms.get(term.utf8ToString());
					// double idf = Math.log(docFreq / numDocs) + 1;
					TermStats ts = new TermStats();
					ts.setTf(tf);
					ts.setIdf(idf);
					termWeight.put(currentTerm, ts);
					// altrimenti, per il caclolo dell'idf si pu� passare ad una similarit� a
					// piacere la docFreq e il numDocs
					// classicSimilarity.idf(docFreq, numDocs)

					// System.out.println("doc:" + docNum + ", term: " + termText + ", termFreq = "
					// + freq);
					// System.out.println("doc:" + docNum + ", term: " + termText + ",
					// inverseTermFreq = " + idf);
					termDocCount++;
				}
			} else {
				String value = field.stringValue();
				Term t = new Term(field.name(), value);
				System.out.println(field.name() + ": " + field.numericValue());
				if (field.numericValue() != null) {
					double fieldValue = Double.parseDouble(value);
					TermStats ts = new TermStats();
					ts.setNumeric(true);
					ts.setNumericValue(fieldValue);
					termWeight.put(t, ts);
				} else {
					System.out.println("Non sono stati salvati i termini del campo \'" + field.name()
							+ "\'. Tutti i termini dei campi devono essere salvati.");
					System.exit(-1);
				}
				termDocCount++;
			}
		}
		for (Term t : termWeight.keySet()) {
			if (!termWeight.get(t).isNumeric()) {
				float tf = termWeight.get(t).getTf();
				termWeight.get(t).setTf(tf / (float) termDocCount);
			}
		}
		return termWeight;
	}

//	private static double norm(HashMap<Term, TermStats> t, boolean sim) {
//		double sum = 0.0;
//		for (Term term : t.keySet()) {
//			if(t.get(term).isNumeric() && sim) {
//				sum += Math.pow(computeSimilarity(t.get(term).getNumericValue(), t.get(term).getNumericValue()), 2);
//			}
//			else {
//				sum += Math.pow(t.get(term).weight(), 2);
//			}
//		}
//		return Math.sqrt(sum);
//	}

	@Deprecated
	public static double cosinSim(HashMap<Term, TermStats> t1, HashMap<Term, TermStats> query, NumericSimilarity sim) {
		Set<Term> tk1 = t1.keySet();
		Set<Term> tk2 = query.keySet();
		double num = 0.0;
		for (Term tt1 : tk1) {
			if (!t1.get(tt1).isNumeric() && query.containsKey(tt1)) {
				num += t1.get(tt1).weight() * query.get(tt1).weight();
			}
			//Questo va bene per entrambi, perch� 1 * sim(d,q) = sim(d,q)
			else if(t1.get(tt1).isNumeric()) {
				String fieldName = tt1.field();
				for (Term tt2 : tk2) {
					if (tt2.field().equals(fieldName)) {
						num += computeSimilarity(t1.get(tt1).getNumericValue(), query.get(tt2).getNumericValue());
						break;
					}
				}
			}
			else if (t1.get(tt1).isNumeric()) {
				String fieldName = tt1.field();
				for (Term tt2 : tk2) {
					if (tt2.field().equals(fieldName)) {
						num += t1.get(tt1).weight() * query.get(tt2).weight();
						break;
					}
				}
			}
		}
		double norm1 = 0.0;
		for (Term term : t1.keySet()) {
			if(t1.get(term).isNumeric() && NumericSimilarity.VECTOR == sim) {
				for(Term qt : query.keySet()) {
					if(term.field().equals(qt.field())) {
						norm1 += Math.pow(computeSimilarity(t1.get(term).getNumericValue(), query.get(qt).getNumericValue()), 2);
					}
				}
			}
			else if(t1.get(term).isNumeric() && NumericSimilarity.COMPUTE == sim) {
				norm1 += Math.pow(computeSimilarity(t1.get(term).getNumericValue(), t1.get(term).getNumericValue()), 2);
			}
			else {
				norm1 += Math.pow(t1.get(term).weight(), 2);
			}
		}
		norm1 = Math.sqrt(norm1);
		double norm2 = 0.0;
		for (Term term : query.keySet()) {
			if(query.get(term).isNumeric() && NumericSimilarity.COMPUTE == sim) {
				norm2 += Math.pow(computeSimilarity(query.get(term).getNumericValue(), query.get(term).getNumericValue()), 2);
			}
			else if (query.get(term).isNumeric() && NumericSimilarity.VECTOR == sim) {
				norm2 += Math.pow(computeSimilarity(query.get(term).getNumericValue(), query.get(term).getNumericValue()), 2);
			}
			else {
				norm2 += Math.pow(query.get(term).weight(), 2);
			}
		}
		norm2 = Math.sqrt(norm2);
		double den = norm1 * norm2;
		System.out.println("numerator: " + num);
		System.out.println("norm(t1): " + norm1);
		System.out.println("norm(query): " + norm2);
		if (num < EPSILON) {
			return 0.0;
		}
		return num / den;
	}

	public static byte[] toByteArray(double value) {
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putDouble(value);
		return bytes;
	}
	
	public static double computeSimilarity(double qValue, double dValue) {
		double distance = Math.abs(qValue - dValue);
		double similarity = 1.0 / (1.0 + distance);
		return similarity;
	}

	/**
	 * 
	 * @param indexManager
	 * @param q
	 * @param docId
	 * @param sim
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	@Deprecated
	public static HashMap<Term, TermStats> getQueryTerms(IndexManager indexManager, Query q, int docId, NumericSimilarity sim)
			throws ParseException, IOException {
		System.out.println("final query: " + q.toString());
		IndexSearcher searcher = indexManager.createSearcher();
		IndexReader reader = indexManager.createReader();
		Set<Term> termQuerySet = new HashSet<Term>();
		Weight w = searcher.createWeight(q, true, 3.4f);
		w.extractTerms(termQuerySet);
		System.out.println("term query set: " + termQuerySet);
		HashMap<Term, TermStats> termsQuery = new HashMap<Term, TermStats>();
		Map<String, Float> idf = Stats.createIdf(reader);
		ArrayList<PointRangeQuery> queryOnNumbers = new ArrayList<PointRangeQuery>();
		BooleanQuery bq = (BooleanQuery) q;
		int queryTermCount = termQuerySet.size();
		for (BooleanClause bc : bq.clauses()) {
			if (bc.getQuery() instanceof PointRangeQuery) {
				PointRangeQuery pointRange = (PointRangeQuery) bc.getQuery();
				String fieldName = pointRange.getField();
//				Document doc = reader.document(docId);
//				IndexableField[] fields = doc.getFields(fieldName);
//				System.out.println(doc.get("titolo"));
//				if (fields != null && fields.length > 0) {
//					System.out.println(NormalizerFactory.getNormalizer("anno")
//							.unNormalize(Double.parseDouble(reader.document(docId).get("anno"))));
//				}
				queryOnNumbers.add(pointRange);
				byte[] lowerp = pointRange.getLowerPoint();
				byte[] upperp = pointRange.getUpperPoint();
				Decoder decoder = DecoderFactory.getDecoder(fieldName);
				Double lowerPoint = decoder.decode(lowerp, 0);
				Double upperPoint = decoder.decode(upperp, 0);
				String docFieldValue = reader.document(docId).get(fieldName);
				Double numericValue = (upperPoint + lowerPoint) / 2.0;
				TermStats ts = new TermStats();
				ts.setNumeric(true);
				ts.setNumericValue(numericValue);
				Term t = new Term(fieldName, numericValue.toString());
				if (docFieldValue != null) {
					Double docFieldDoubleValue = Double.parseDouble(docFieldValue);
					if (docFieldDoubleValue <= upperPoint && docFieldDoubleValue >= lowerPoint) {
						numericValue = docFieldDoubleValue;
					}
					else if(docFieldDoubleValue - upperPoint < docFieldDoubleValue - lowerPoint) {
						numericValue = upperPoint;
					}
					else {
						numericValue = lowerPoint;
					}
					ts = new TermStats();
					ts.setNumeric(true);
					ts.setNumericValue(numericValue);
					t = new Term(fieldName, docFieldValue);
				}
				termsQuery.put(t, ts);
				queryTermCount++;
			}
		}
		for (Term term : termQuerySet) {
			if (reader.document(0).getField(term.field()).numericValue() == null && idf.containsKey(term.text())) {
				float freqTerm = 0;
				for (Term term1 : termQuerySet) {
					if (term.equals(term1)) {
						freqTerm++;
					}
				}
				freqTerm = freqTerm / (float) queryTermCount;
				TermStats ts = new TermStats();
				ts.setTerm(term);
				ts.setTf(freqTerm);
				ts.setIdf(idf.get(term.text()));
				termsQuery.put(term, ts);
			} else if (reader.document(0).getField(term.field()).numericValue() != null) {
				// TermStats ts = new TermStats();
				// ts.setTerm(term);
				// ts.setTf((float) freqTerm);
				// ts.setIdf(idf.get(term.text()));
				// termsQuery.put(term, ts);
				System.out.println("Perch� sono qui se � un termine??" + term);
				System.exit(-1);
			} else {
				System.out.println("almeno un termine della query non � mai presente nei documenti");
			}
		}
		// Weight w = new ConstantScoreWeight(q, 3.4f);
		return termsQuery;
	}

}
