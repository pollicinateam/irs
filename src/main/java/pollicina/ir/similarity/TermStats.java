package pollicina.ir.similarity;

import org.apache.lucene.index.Term;

public class TermStats {
	
	private static final Double EPSILON = Math.pow(10, -9);
	
	private Term term;
	
	private Float tf;
	private Float idf;
	
	private boolean numeric;
	private Double numericValue;
	
	public TermStats() {
		this.numeric = false;
		this.numericValue = null;
	}

	public Float getTf() {
		return tf;
	}

	public void setTf(Float tf) {
		this.tf = tf;
	}

	public Float getIdf() {
		return idf;
	}

	public void setIdf(Float idf) {
		this.idf = idf;
	}

	public Term getTerm() {
		return term;
	}
	public void setTerm(Term term) {
		this.term = term;
	}
	
	public double tfIdf() {
		return tf * idf;
//		return 1.0 * idf;
	}
	public boolean isNumeric() {
		return numeric;
	}
	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}
	public Double getNumericValue() {
		return numericValue;
	}
	public void setNumericValue(Double numericValue) {
		this.numericValue = numericValue;
	}

	public double weight() {
		if(isNumeric()) {
			return numericValue;
		}
		return tfIdf();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idf == null) ? 0 : idf.hashCode());
		result = prime * result + (numeric ? 1231 : 1237);
		result = prime * result + ((numericValue == null) ? 0 : numericValue.hashCode());
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		result = prime * result + ((tf == null) ? 0 : tf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermStats other = (TermStats) obj;
		if (idf == null) {
			if (other.idf != null)
				return false;
		} else if (!idf.equals(other.idf))
			return false;
		if (numeric != other.numeric)
			return false;
		if (numericValue == null) {
			if (other.numericValue != null)
				return false;
		} else if (!numericValue.equals(other.numericValue))
			return false;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.equals(other.term))
			return false;
		if (tf == null) {
			if (other.tf != null)
				return false;
		} else if (!tf.equals(other.tf))
			return false;
		return true;
	}

}
