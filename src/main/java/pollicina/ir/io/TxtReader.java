package pollicina.ir.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TxtReader {
	
	private static final String PATH = "paisa.raw.utf8";
	private static final String DESTINATION = "paisapart";

	public static ArrayList<String> read(int maxLines) throws IOException {
		FileReader fr = null;
		BufferedReader br = null;
		ArrayList<String> lines = new ArrayList<String>();
		try {
			fr = new FileReader(PATH);
			br = new BufferedReader(fr);
			int lineCounter = 0;
			while(br.readLine() != null && lineCounter < maxLines) {
				String line = br.readLine();
				System.out.println(line);
				lines.add(line);
				lineCounter++;
			}
		} finally {
			if(br != null) {
				br.close();
			}
			if(fr != null) {
				fr.close();
			}
		}
		return lines;
	}
	
	public static void write(ArrayList<String> lines, String filename) throws IOException {
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(filename);
			bw = new BufferedWriter(fw);
			for(int i = 0; i < lines.size(); i++) {
				fw.write(lines.get(i));
			}
		} finally {
			if(bw != null) {
				bw.close();
			}
			if(fw != null) {
				fw.close();
			}
		}
	}
	
}
