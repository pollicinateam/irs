package pollicina.ir.io;

import java.io.IOException;
import java.util.HashMap;

public class IndexManagerFactory {
	
	private static final String PATH_BOOKS_REPO = "LibriRepository";
	private static final String PATH_POLLI_INDEX = "pollicinaIndex";
	
	private static final String PATH_BOOKS_UNIGRAM = "LibriUnigram";
	private static final String PATH_POLLI_INDEX_UNIGRAM = "pollicinaIndexUnigram";
	
	private static HashMap<String, IndexManager> store;
	
	public static IndexManager getPolliIndexManager() throws IOException {
		return getIndexManager(PATH_POLLI_INDEX);
	}
	
	public static IndexManager getPolliUniIndexManager() throws IOException {
		return getIndexManager(PATH_POLLI_INDEX_UNIGRAM);
	}
	
	public static IndexManager getBooksIndexManager() throws IOException {
		return getIndexManager(PATH_BOOKS_REPO);
	}
	
	public static IndexManager getBooksUniIndexManager() throws IOException {
		return getIndexManager(PATH_BOOKS_UNIGRAM);
	}
	
	public static synchronized IndexManager getIndexManager(String path) throws IOException {
		if(store == null) {
			store = new HashMap<String, IndexManager>();
		}
		if(!store.containsKey(path)) {
			store.put(path, createIndexManager(path));
		}
		return store.get(path);
	}
	
	private static IndexManager createIndexManager(String path) throws IOException {
		IndexManager im = new IndexManager();
		im.setRepositoryPath(path);
		return im;
	}

}
