package pollicina.ir.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class IndexManager {
	
	private IndexSearcher searcher;
	private DirectoryReader reader;
	private String repositoryPath;
	
	public synchronized DirectoryReader createReader() throws IOException {
		if(reader == null) {
			Directory dir = FSDirectory.open(new File(repositoryPath).toPath());
			DirectoryReader reader = DirectoryReader.open(dir);
			setReader(reader);
		}
		return reader;
	}
	
	public synchronized IndexSearcher createSearcher() throws IOException {
		createReader();
		if(searcher == null) {
			IndexSearcher searcher = new IndexSearcher(reader);
			setSearcher(searcher);
		}
		return searcher;
	}
	
	public void writeIndex(ArrayList<Document> documents, Analyzer analyzer) throws IOException {
		
		IndexWriter writer = null;
		File repo = null;
		
		try {
			
			repo = new File(getRepositoryPath());
			IndexWriterConfig indexConf = new IndexWriterConfig(analyzer);
			indexConf.setCommitOnClose(true);
			indexConf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
			
			Directory dir = FSDirectory.open(repo.toPath());
			
			writer = new IndexWriter(dir, indexConf);
			
			writer.addDocuments(documents);
			
			writer.flush();
			writer.commit();
			writer.close();
			writer = null;
			
		} catch(IOException e) {
			if(writer != null) {
				writer.rollback();
			}
			throw e;
		} finally {
			if(writer != null) {
				writer.close();
			}
		}
		
	}

	public void setReader(DirectoryReader reader) {
		this.reader = reader;
	}

	public String getRepositoryPath() {
		return repositoryPath;
	}

	public void setRepositoryPath(String repositoryPath) {
		this.repositoryPath = repositoryPath;
	}

	public void setSearcher(IndexSearcher searcher) {
		this.searcher = searcher;
	}

}
