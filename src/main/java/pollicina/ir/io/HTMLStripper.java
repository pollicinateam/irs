package pollicina.ir.io;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;

public class HTMLStripper {
	
	public static String filterHTML(Reader reader) throws IOException {
		HTMLStripCharFilter html  = null;
		StringBuilder sb = null;
		try {
			html = new HTMLStripCharFilter(reader);
			sb = new StringBuilder();
			int f = -1;
			while((f = html.read()) != -1) {
				char ch = (char) f;
				sb.append(ch);
			}
			html.close();
			html = null;
		}finally {
			if(html != null) {
				html.close();
			}
		}
		return sb.toString();
	}
	
	public static String filterHTML(String phrase) throws IOException {
		StringReader sr = null;
		try {
			sr = new StringReader(phrase);
			return filterHTML(sr);
		}finally {
			if(sr != null) {
				sr.close();
			}
		}
	}

}
