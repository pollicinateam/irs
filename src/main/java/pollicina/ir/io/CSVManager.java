package pollicina.ir.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.opencsv.CSVReader;

public class CSVManager {
	
	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';
	private boolean rowIndeces;
	
	public CSVManager() {
		rowIndeces = false;
	}
	
	public CSVManager(boolean rowIndeces) {
		this.rowIndeces = rowIndeces;
	}
	
	/**
	 * This method reads from file csv "datasetNameinfo.csv" the info of the dataset (index attribute and whether it is numeric)
	 * @param datasetName, the name of the dataset
	 * @return the map containing the index of the attribute as key and whether it is numeric as value
	 * @throws IOException
	 */
	public HashMap<Integer, Boolean> readDsInfo(String datasetName) throws IOException{
		HashMap<Integer, Boolean> infos = null;
		ArrayList<ArrayList<String>> infoLines = new ArrayList<ArrayList<String>>();
		infos = new HashMap<Integer, Boolean>();
		infoLines = readLines(datasetName+"info");
		for(int i = 0; i < infoLines.size(); i++) {
			int attIndx = Integer.parseInt(infoLines.get(i).get(0));
			boolean numeric = Boolean.parseBoolean(infoLines.get(i).get(1));
			infos.put(attIndx, numeric);
		}
		return infos;		
	}
	
	public static List<String> parseLine(String csvLine){
		return parseLine(csvLine, CSVManager.DEFAULT_SEPARATOR, CSVManager.DEFAULT_QUOTE);
	}
	
    public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

        List<String> result = new ArrayList<String>();

        //if empty, return!
        if (cvsLine == null || cvsLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = DEFAULT_QUOTE;
        }

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;

        char[] chars = cvsLine.toCharArray();

        for (char ch : chars) {

            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                } else {

                    //Fixed : allow "" in custom quote enclosed
                    if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }

                }
            } else {
                if (ch == customQuote) {

                    inQuotes = true;

                    //Fixed : allow "" in empty quote enclosed
                    if (chars[0] != '"' && customQuote == '\"') {
                        curVal.append('"');
                    }

                    //double quotes in column will hit this!
                    if (startCollectChar) {
                        curVal.append('"');
                    }

                } else if (ch == separators) {

                    result.add(curVal.toString());

                    curVal = new StringBuffer();
                    startCollectChar = false;

                } else if (ch == '\r') {
                    //ignore LF characters
                    continue;
                } else if (ch == '\n') {
                    //the end, break!
                    break;
                } else {
                    curVal.append(ch);
                }
            }

        }

        result.add(curVal.toString());

        return result;
    }
	
	/**
	 * 
	 * @param datasetName
	 * @return
	 * @throws IOException
	 * 
	 * This method takes as input a dataset name. Looks for a csvFile with this name and, if present, reads it and returns lines.
	 * Moreover, add to the head of each line returned a rownumber.
	 */
	public ArrayList<ArrayList<String>> readDataset(String datasetName) throws IOException {
		String fileName = datasetName+".csv";
		ArrayList<ArrayList<String>> dataset = null;
//		BufferedReader br = null;
		FileReader fr = null;
		CSVReader csvr = null;
		try {
			fr = new FileReader(fileName);
			csvr = new CSVReader(fr);
//			br = new BufferedReader(fr);
			dataset = new ArrayList<ArrayList<String>>();
//			String line = "";
			Long lineCount = 1L;
			String[] nextRecord = null;
			while((nextRecord = csvr.readNext()) != null) {
				ArrayList<String> record = new ArrayList<String>();
				for(int i = 0; i < nextRecord.length; i++) {
					record.add(nextRecord[i].toString());
				}
				lineCount++;
				dataset.add(record);
			}
//	        while ((line = br.readLine()) != null) {
//	        	ArrayList<String> record = new ArrayList<String>();
////	        	String[] instance = line.split(cvsSplitBy);
//	        	List<String> instance = parseLine(line);
//	        	//TODO Delete following line if you will use csv with row number
////        		record.add(lineCount.toString());
//	        	if(instance.size() != 15) {
//	        		System.out.println(instance.size());
//	        	}
//	        	for(int i = 0; i < instance.size(); i++) {
//	        		record.add(instance.get(i));
//	        	}
//	        	dataset.add(record);
//	        	lineCount++;
//	        }
//	        br.close();
//	        br = null;
			csvr.close();
			csvr = null;
	        fr.close();
	        fr = null;
		}finally {
//			if(br != null) {
//				br.close();
//				br = null;
//			}
			if(csvr != null) {
				csvr.close();
			}
			if(fr != null) {
				fr.close();
				fr = null;
			}
		}
        return dataset;
	}
	
	public ArrayList<ArrayList<String>> readLines(String fileName) throws IOException {
		String csvFileName = fileName+".csv";
		ArrayList<ArrayList<String>> dataset = null;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(csvFileName));
			dataset = new ArrayList<ArrayList<String>>();
			String line = "";
			String cvsSplitBy = ",";
			Long lineCount = 1L;
	        while ((line = br.readLine()) != null) {
	        	ArrayList<String> record = new ArrayList<String>();
	        	String[] instance = line.split(cvsSplitBy);
	        	for(int i = 0; i < instance.length; i++) {
	        		record.add(instance[i]);
	        	}
	        	dataset.add(record);
	        	lineCount++;
	        }
	        br.close();
	        br = null;
		}finally {
			if(br != null) {
				br.close();
				br = null;
			}
		}
        return dataset;
	}
	
    public void writeLine(Writer w, List<String> values) throws IOException {
        writeLine(w, values, DEFAULT_SEPARATOR, ' ');
    }

    public void writeLine(Writer w, List<String> values, char separators) throws IOException {
        writeLine(w, values, separators, ' ');
    }

    private String followCVSformat(String value) {

        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    public void writeLine(Writer w, List<String> values, char separators, char customQuote) throws IOException {

        boolean first = true;

        //default customQuote is empty

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(separators);
            }
            if (customQuote == ' ') {
                sb.append(followCVSformat(value));
            } else {
                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }

            first = false;
        }
        sb.append("\n");
        w.append(sb.toString());


    }
    
    public void writeLines(String filename, ArrayList<ArrayList<String>> lines, boolean append) throws IOException{
    	String csvFile = filename + ".csv";
    	FileWriter fWriter = new FileWriter(csvFile, append);
    	for(int i = 0; i < lines.size(); i++){
    		writeLine(fWriter, lines.get(i));
    	}
    	fWriter.flush();
    	fWriter.close();
    }

	public boolean isRowIndeces() {
		return rowIndeces;
	}

	public void setRowIndeces(boolean rowIndeces) {
		this.rowIndeces = rowIndeces;
	}

}
