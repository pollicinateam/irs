package pollicina.ir.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.it.ItalianAnalyzer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class StopWordsManager {
	
	private static final String BASE_PATH = "C:\\Users\\Matteo\\Documents\\Ricerca\\InformationRetrieval\\Laboratorio\\ItalianStopwords\\";
	
	private static final String[] FILES = {"it.json", "stopwords.txt", "stopwords-it.json"};
//	private static final String[] FILES = {"stop.txt"};
	
	private static StopWordsManager instance;
	private CharArraySet stopwords;
	
	private StopWordsManager() {
		
	}
	
	public static CharArraySet getAllStops() throws IOException {
		CharArraySet defStops = ItalianAnalyzer.getDefaultStopSet();
		CharArraySet additionalStops = StopWordsManager.subStop(StopWordsManager.getInstance().getStopwords(), defStops);
		CharArraySet stops = StopWordsManager.mergeStop(defStops, additionalStops);
		return stops;
	}
	
	public static synchronized StopWordsManager getInstance() throws IOException {
		if(instance == null) {
			instance = new StopWordsManager();
			instance.readStopWords();
		}
		return instance;
	}
	
//	public void removeAllFrom(CharArraySet stopwords) {
//		CharArraySet tempStop = null;
//		for(int i = 0; i < this.stopwords.size(); i++) {
//			stopwords.remove(o)
//		}
//	}
	
	public void readStopWords() throws IOException {
		ArrayList<char[]> stopwords = new ArrayList<char[]>();
		for(int i = 0; i < FILES.length; i++) {
			String fileName = FILES[i];
			if(fileName.endsWith(".txt")) {
				stopwords.addAll(readTxt(fileName));
			}
			else if(fileName.endsWith(".json")) {
				stopwords.addAll(readJson(fileName));
			}
			else {
				System.out.println("type of file not managed");
				System.exit(-1);
			}
		}
		this.stopwords = new CharArraySet(stopwords.size(), true);
		for(int i = 0; i < stopwords.size(); i++) {
			this.stopwords.add(stopwords.get(i));
		}
	}
	
	public static CharArraySet mergeStop(CharArraySet m1, CharArraySet m2) {
		ArrayList<String> fromStr = new ArrayList<String>();
		Iterator it = m1.iterator();
		while(it.hasNext()) {
			char[] stop = (char[]) it.next();
			fromStr.add(String.valueOf(stop));
		}
		ArrayList<String> toStr = new ArrayList<String>();
		Iterator it1 = m2.iterator();
		while(it1.hasNext()) {
			char[] stop = (char[]) it1.next();
			toStr.add(String.valueOf(stop));
		}
		fromStr.addAll(toStr);
		CharArraySet result = new CharArraySet(fromStr, true);
		return result;
	}
	
	public static CharArraySet subStop(CharArraySet from, CharArraySet stops) {
		ArrayList<String> fromStr = new ArrayList<String>();
		Iterator it = from.iterator();
		while(it.hasNext()) {
			char[] stop = (char[]) it.next();
			fromStr.add(String.valueOf(stop));
		}
		ArrayList<String> toStr = new ArrayList<String>();
		Iterator it1 = stops.iterator();
		while(it1.hasNext()) {
			char[] stop = (char[]) it1.next();
			toStr.add(String.valueOf(stop));
		}
		fromStr.removeAll(toStr);
		CharArraySet result = new CharArraySet(fromStr, true);
		return result;
	}
	

	public static ArrayList<char[]> readJson(String fileName) throws IOException {
		JsonParser parser = new JsonParser();
		FileReader fr = null;
		ArrayList<char[]> stopwords = new ArrayList<char[]>();
		try {
			fr = new FileReader(BASE_PATH + fileName); 
			JsonReader reader = new JsonReader(fr);
			JsonElement root = parser.parse(reader);
			JsonArray elements = root.getAsJsonArray();
			for(int i = 0; i < elements.size(); i++) {
				stopwords.add(elements.get(i).getAsString().trim().toCharArray());
			}
			fr.close();
			fr = null;
		}finally {
			if(fr != null) {
				fr.close();
			}
		}
		return stopwords;
	}
	
	public static ArrayList<char[]> readTxt(String fileName) throws IOException {
		FileReader f = null;
		BufferedReader br = null;
		ArrayList<char[]> stopwords = new ArrayList<char[]>();
		try {
			f = new FileReader(BASE_PATH + fileName);
			br = new BufferedReader(f);
			String line = null;
			while((line = br.readLine()) != null) {
				stopwords.add(line.trim().toCharArray());
			}
			br.close();
			f.close();
		} finally {
			if(br != null) {
				br.close();
			}
			if(f != null) {
				f.close();
			}
		}
		return stopwords;
	}
	
	public static void printStops(CharArraySet stopws) {
		Iterator<Object> it = stopws.iterator();
		while(it.hasNext()) {
			System.out.println((char[]) it.next());
		}
	}

	public CharArraySet getStopwords() {
		return stopwords;
	}

	public void setStopwords(CharArraySet stopwords) {
		this.stopwords = stopwords;
	}

}
