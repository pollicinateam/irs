package pollicina.ir.mapper;

import javax.annotation.Generated;
import pollicina.dto.ArtworkDto;
import pollicina.ir.dto.Artwork;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-07-08T15:59:42+0200",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_171 (Oracle Corporation)"
)
public class ArtworkMapperImpl implements ArtworkMapper {

    @Override
    public ArtworkDto mapToDto(Artwork artwork) {
        if ( artwork == null ) {
            return null;
        }

        ArtworkDto artworkDto = new ArtworkDto();

        artworkDto.setTitle( artwork.getTitle() );
        artworkDto.setDescription( artwork.getDescription() );
        artworkDto.setNsc( artwork.getNsc() );

        return artworkDto;
    }

    @Override
    public Artwork mapFromDto(ArtworkDto artwork) {
        if ( artwork == null ) {
            return null;
        }

        Artwork artwork1 = new Artwork();

        artwork1.setTitle( artwork.getTitle() );
        artwork1.setDescription( artwork.getDescription() );
        artwork1.setNsc( artwork.getNsc() );

        return artwork1;
    }
}
