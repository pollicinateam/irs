package pollicina.ir.mapper;

import org.mapstruct.Mapper;

import pollicina.dto.ArtworkDto;
import pollicina.ir.dto.Artwork;

@Mapper
public interface ArtworkMapper {
	
	public ArtworkDto mapToDto(Artwork artwork);
	
	public Artwork mapFromDto(ArtworkDto artwork);

}
