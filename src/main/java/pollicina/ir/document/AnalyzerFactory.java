package pollicina.ir.document;

import java.io.IOException;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.shingle.ShingleAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import pollicina.ir.document.analyzer.MyAnalyzer;
import pollicina.ir.io.StopWordsManager;

public class AnalyzerFactory {
	
	
	public static Analyzer getItalianAnalyzer() throws IOException {
		CharArraySet defStops = ItalianAnalyzer.getDefaultStopSet();
		CharArraySet additionalStops = StopWordsManager.subStop(StopWordsManager.getInstance().getStopwords(), defStops);
		CharArraySet stops = StopWordsManager.mergeStop(defStops, additionalStops);
//		StopWordsManager.printStops(stops);
		ItalianAnalyzer italian = new ItalianAnalyzer(stops);
		return italian;
	}
	
	public static Analyzer createPollyCustomAnalyzer() throws IOException {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
		MyAnalyzer italian = new MyAnalyzer();
		
		ShingleAnalyzerWrapper italianShingle = new ShingleAnalyzerWrapper(italian, 2, 3);
		
		analyzerMap.put("title", italianShingle);
		
		analyzerMap.put("description", italianShingle);
		
		analyzerMap.put("nsc", italianShingle);
		
		PerFieldAnalyzerWrapper perFieldAnalyzer = new PerFieldAnalyzerWrapper(new ItalianAnalyzer(), analyzerMap);
		
		return perFieldAnalyzer;
	}
	
	public static Analyzer createPollyUniCustomAnalyzer() throws IOException {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
		MyAnalyzer italian = new MyAnalyzer();
		
		analyzerMap.put("title", italian);
		
		analyzerMap.put("description", italian);
		
		analyzerMap.put("nsc", italian);
		
		PerFieldAnalyzerWrapper perFieldAnalyzer = new PerFieldAnalyzerWrapper(new ItalianAnalyzer(), analyzerMap);
		
		return perFieldAnalyzer;
	}
	
	public static Analyzer createArtworkAnalyzer() throws IOException {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
//		ItalianAnalyzer italian = (ItalianAnalyzer) getItalianAnalyzer();
		
		MyAnalyzer italian = new MyAnalyzer();
		
		ShingleAnalyzerWrapper italianShingle = new ShingleAnalyzerWrapper(italian, 2, 3);
		
		analyzerMap.put("title", italianShingle);
		
		analyzerMap.put("description", italianShingle);
		
		analyzerMap.put("nsc", italianShingle);
		
		PerFieldAnalyzerWrapper perFieldAnalyzer = new PerFieldAnalyzerWrapper(new ItalianAnalyzer(), analyzerMap);
		
		return perFieldAnalyzer;
	}
	
	public static Analyzer createUniArtworkAnalyzer() throws IOException {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
//		ItalianAnalyzer italian = (ItalianAnalyzer) getItalianAnalyzer();
		MyAnalyzer italian = new MyAnalyzer();
		
		analyzerMap.put("title", italian);
		
		analyzerMap.put("description", italian);
		
		analyzerMap.put("nsc", italian);
		
		PerFieldAnalyzerWrapper perFieldAnalyzer = new PerFieldAnalyzerWrapper(new ItalianAnalyzer(), analyzerMap);
		
		return perFieldAnalyzer;
	}
	
	public static Analyzer createUniBookAnalyzer() throws IOException {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
		KeywordAnalyzer keyword = new KeywordAnalyzer();
		
//		ItalianAnalyzer italian = (ItalianAnalyzer) getItalianAnalyzer();
		Analyzer italian = new MyAnalyzer();
		
		analyzerMap.put("autore", italian);
		
		analyzerMap.put("titolo", italian);
		
		analyzerMap.put("casa", italian);
		
		analyzerMap.put("genere", italian);
		
		analyzerMap.put("lingua", italian);
		
		analyzerMap.put("anno", keyword);
		
//		analyzerMap.put("note", keyword);
		analyzerMap.put("note", italian);
		
		
		PerFieldAnalyzerWrapper perFieldAnalyzer = new PerFieldAnalyzerWrapper(new ItalianAnalyzer(), analyzerMap);
		
		return perFieldAnalyzer;
	}
	
	
	public static Analyzer createBookAnalyzer() throws IOException {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
		KeywordAnalyzer keyword = new KeywordAnalyzer();
		
//		CharArraySet defStops = ItalianAnalyzer.getDefaultStopSet();
//		CharArraySet additionalStops = StopWordsManager.subStop(StopWordsManager.getInstance().getStopwords(), defStops);
//		CharArraySet stops = StopWordsManager.mergeStop(defStops, additionalStops);
////		StopWordsManager.printStops(stops);
//		ItalianAnalyzer italian = (ItalianAnalyzer) getItalianAnalyzer();
		Analyzer italian = new MyAnalyzer();
		
		ShingleAnalyzerWrapper shingle = new ShingleAnalyzerWrapper(italian, 2, 3);
		
		analyzerMap.put("autore", shingle);
		
		analyzerMap.put("titolo", shingle);
		
		analyzerMap.put("casa", shingle);
		
		analyzerMap.put("genere", shingle);
		
		analyzerMap.put("lingua", shingle);
		
		analyzerMap.put("anno", keyword);
		
//		analyzerMap.put("note", keyword);
		analyzerMap.put("note", italian);
		
		
		PerFieldAnalyzerWrapper perFieldAnalyzer = new PerFieldAnalyzerWrapper(new ItalianAnalyzer(), analyzerMap);
		
		return perFieldAnalyzer;
	}

	public static Analyzer createMovieAnalyzer() {
		HashMap<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		
		KeywordAnalyzer keyword = new KeywordAnalyzer();
		StandardAnalyzer english = new StandardAnalyzer();
		
		//analyzer for url
		analyzerMap.put("url", keyword);
		
		//analyzer for title
//		analyzerMap.put("title", new ShingleAnalyzerWrapper(english, 2, 3));
		analyzerMap.put("title", new StandardAnalyzer());
		
		//analyzer for author
		analyzerMap.put("author", new ShingleAnalyzerWrapper(english, 2, 3));
		
		String[] keywords = {"price", "save", "pages", "size", "publisher", "language", "text_to_speech", "x_ray", "lending", "customer_reviews", "stars"};
		for(int i = 0; i < keywords.length; i++) {
			analyzerMap.put(keywords[i], keyword);
		}
		
		//analyzer for description
		analyzerMap.put("description", new ShingleAnalyzerWrapper(english, 2, 3));
		
		
		PerFieldAnalyzerWrapper perField = new PerFieldAnalyzerWrapper(new StandardAnalyzer(), analyzerMap);
		return perField;
		
	}
	
}
