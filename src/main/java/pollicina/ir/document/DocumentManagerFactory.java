package pollicina.ir.document;

public class DocumentManagerFactory {
	
	private static final String[] MOVIE_FIELDS = {"url", "title", "author", "price", "save", "pages", "description", "publisher", "language"};
	private static final String[] BOOK_FIELDS = {"titolo", "autore", "casa", "genere", "lingua", "anno", "note"};
	private static final String[] ARTWORK_FIELDS = {"title", "description", "nsc"};
	
	public static DocumentManager getMovieManager() {
		return getManager(MOVIE_FIELDS);
	}
	
	public static DocumentManager getBookManager() {
		return getManager(BOOK_FIELDS);
	}
	
	public static DocumentManager getArtworkManager () {
		return getManager(ARTWORK_FIELDS);
	}
	
	public static DocumentManager getManager(String[] fields) {
		DocumentManager docM = new DocumentManager();
		docM.setFields(fields);
		return docM;
	}

}
