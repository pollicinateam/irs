package pollicina.ir.document;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import pollicina.ir.datasetmanipulation.Normalizer;
import pollicina.ir.datasetmanipulation.NormalizerFactory;
import pollicina.ir.dto.Artwork;
import pollicina.ir.io.CSVManager;
import pollicina.ir.io.HTMLStripper;

public class DocumentManager {
	
//	private static final String[] PRINT_FIELDS = {"url", "title", "author", "price", "save", "pages", "description", "publisher", "language"};
	
	private String[] fields;
	
	private String decode(String value, String fieldName, Document doc) {
		if(doc.getField(fieldName) != null && doc.getField(fieldName).numericValue() != null) {
			Normalizer normalizer = NormalizerFactory.getNormalizer(fieldName);
			String newValue = normalizer.unNormalize(Double.parseDouble(value)).toString();
			return newValue;
		}
		return value;
	}
	
	public String docToString(Document doc) {
		StringBuilder sB = new StringBuilder();
		for(int i = 0; i < fields.length; i++) {
			sB.append(fields[i]);
			sB.append(": ");
			sB.append(decode(doc.get(fields[i]), fields[i], doc));
			sB.append("\n");
		}
		return sB.toString();
	}
	
	public void printDocs(TopDocs topDocs, IndexSearcher searcher) throws IOException {
		ScoreDoc[] docs = topDocs.scoreDocs;
		for(int i = 0; i < docs.length; i++) {
			System.out.println(docs[i].doc);
			Document doc = searcher.doc(docs[i].doc);
			System.out.println(docToString(doc));
		}
	}
	
	public ArrayList<Document> createArtworkDocuments(ArrayList<Artwork> artworks) throws IOException {
		ArrayList<Document> artDocs = new ArrayList<Document>();
		FieldType indicizedType = new FieldType();
		indicizedType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
		indicizedType.setStoreTermVectors(true);
		indicizedType.setStoreTermVectorPositions(true);
		indicizedType.setStoreTermVectorPayloads(true);
//		type.setTokenized(true);
		indicizedType.setStored(true);
		for(int i = 0; i < artworks.size(); i++) {
			String title = artworks.get(i).getTitle();
			String description = artworks.get(i).getDescription();
			String nsc = artworks.get(i).getNsc();
			Document doc = new Document();
			if(title != null && !title.isEmpty()) {
				String strippedTitle = HTMLStripper.filterHTML(title);
				Field titleField = new Field("title", strippedTitle, indicizedType);
				doc.add(titleField);
			}
			if(description != null && !description.isEmpty()) {
				String strippedDescritpion = HTMLStripper.filterHTML(description);
				Field descriptionField = new Field("description", strippedDescritpion, indicizedType);
				doc.add(descriptionField);
			}
			if(nsc != null && !nsc.isEmpty()) {
				String strippedNsc = HTMLStripper.filterHTML(nsc);
				Field nscField = new Field("nsc", strippedNsc, indicizedType);
				doc.add(nscField);
			}
			artDocs.add(doc);
		}
		return artDocs;
	}
	
	public static ArrayList<Document> createBooksDocuments() throws IOException {
		CSVManager csvm = new CSVManager();
		ArrayList<ArrayList<String>> dataset = csvm.readDataset("libri");
		ArrayList<Document> documents = new ArrayList<Document>();
		FieldType type = new FieldType();
		type.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
		type.setStoreTermVectors(true);
		type.setStoreTermVectorPositions(true);
		type.setStoreTermVectorPayloads(true);
//		type.setTokenized(true);
		type.setStored(true);
		for(int i = 1; i < dataset.size(); i++) {
			Document doc = new Document();
			if(dataset.get(i).get(1) != null && !dataset.get(i).get(1).isEmpty()) {
				Field f = new Field("autore", dataset.get(i).get(1), type);
				doc.add(f);
			}
			if(dataset.get(i).get(2) != null && !dataset.get(i).get(2).isEmpty()) {
				Field f = new Field("titolo", dataset.get(i).get(2), type);
				doc.add(f);
			}
			if(dataset.get(i).get(3) != null && !dataset.get(i).get(3).isEmpty()) {
				Field f = new Field("casa", dataset.get(i).get(3), type);
				doc.add(f);
			}
			if(dataset.get(i).get(4) != null && !dataset.get(i).get(4).isEmpty()) {
				Field f = new Field("genere", dataset.get(i).get(4), type);
				doc.add(f);
			}
			if(dataset.get(i).get(5) != null && !dataset.get(i).get(5).isEmpty()) {
//				Field f = new StringField("lingua", dataset.get(i).get(5), Store.YES);
//				doc.add(f);
				Field f = new Field("lingua", dataset.get(i).get(5), type);
				doc.add(f);
			}
			if(dataset.get(i).get(6) != null && !dataset.get(i).get(6).isEmpty()) {
				int anno = Integer.parseInt(dataset.get(i).get(6));
				Field f = new IntPoint("anno", anno);
				Field sf = new StoredField("anno", anno);
				sf.setIntValue(anno);
				doc.add(f);
				doc.add(sf);
			}
			if(dataset.get(i).get(7) != null && !dataset.get(i).get(7).isEmpty()) {
//				Field f = new StringField("note", dataset.get(i).get(7), Store.YES);
//				doc.add(f);
				Field f = new Field("note", dataset.get(i).get(7), type);
				doc.add(f);
			}
			documents.add(doc);
		}
		return documents;
	}

	public static ArrayList<Document> createMovieDocuments() throws IOException{
		CSVManager csv = new CSVManager();
		ArrayList<ArrayList<String>> dataset = csv.readDataset("movies");
		ArrayList<Document> documents = new ArrayList<Document>();
		FieldType type = new FieldType();
		type.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
		type.setStoreTermVectors(true);
		type.setStoreTermVectorPositions(true);
		type.setStoreTermVectorPayloads(true);
		type.setTokenized(true);
		type.setStored(true);
		for(int i = 1; i < dataset.size(); i++) {
			Document doc = new Document();
			if(dataset.get(i).get(0) != null && !dataset.get(i).get(0).isEmpty())
				doc.add(new StringField("url", dataset.get(i).get(0), Field.Store.YES));
			if(dataset.get(i).get(1) != null && !dataset.get(i).get(1).isEmpty()) {
				Field f = new Field("title", dataset.get(i).get(1), type);
				doc.add(f);
			}
			if(dataset.get(i).get(2) != null && !dataset.get(i).get(2).isEmpty()) {
				doc.add(new Field("author", dataset.get(i).get(2), type));
			}
			if(dataset.get(i).get(3) != null && !dataset.get(i).get(3).isEmpty()) {
				doc.add(new DoublePoint("price", Double.parseDouble(dataset.get(i).get(3))));
				StoredField sf = new StoredField("price", Double.parseDouble(dataset.get(i).get(3)));
				doc.add(sf);
			}
			if(dataset.get(i).get(4) != null && !dataset.get(i).get(4).isEmpty()) {
				//check null
				doc.add(new DoublePoint("save", Double.parseDouble(dataset.get(i).get(4))));
				StoredField sf = new StoredField("save", Double.parseDouble(dataset.get(i).get(4)));
				doc.add(sf);
			}
			if(dataset.get(i).get(5) != null && !dataset.get(i).get(5).isEmpty()) {
				doc.add(new IntPoint("pages", Integer.parseInt(dataset.get(i).get(5))));
				StoredField sf = new StoredField("pages", Double.parseDouble(dataset.get(i).get(5)));
				doc.add(sf);
			}
			if(dataset.get(i).get(6) != null && !dataset.get(i).get(6).isEmpty()) {
				doc.add(new Field("description", dataset.get(i).get(6), type));
			}
			if(dataset.get(i).get(7) != null && !dataset.get(i).get(7).isEmpty()) {
				doc.add(new DoublePoint("size", Double.parseDouble(dataset.get(i).get(7))));
				StoredField sf = new StoredField("size", Double.parseDouble(dataset.get(i).get(7)));
				doc.add(sf);
			}
			if(dataset.get(i).get(8) != null && !dataset.get(i).get(8).isEmpty()) {
				doc.add(new Field("publisher", dataset.get(i).get(8), type));
			}
			if(dataset.get(i).get(9) != null && !dataset.get(i).get(9).isEmpty())
				doc.add(new StringField("language", dataset.get(i).get(9), Field.Store.YES));
			if(dataset.get(i).get(10) != null && !dataset.get(i).get(10).isEmpty())
				doc.add(new StringField("text_to_speech", dataset.get(i).get(10), Field.Store.YES));
			if(dataset.get(i).get(11) != null && !dataset.get(i).get(11).isEmpty())
				doc.add(new StringField("x_ray", dataset.get(i).get(11), Field.Store.YES));
			if(dataset.get(i).get(12) != null && !dataset.get(i).get(12).isEmpty())
				doc.add(new StringField("lending", dataset.get(i).get(12), Field.Store.YES));
			if(dataset.get(i).get(13) != null && !dataset.get(i).get(13).isEmpty()) {
				doc.add(new DoublePoint("customer_reviews", Double.parseDouble(dataset.get(i).get(13))));
				StoredField sf = new StoredField("customer_reviews", Double.parseDouble(dataset.get(i).get(13)));
				doc.add(sf);
			}
			if(dataset.get(i).get(14) != null && !dataset.get(i).get(14).isEmpty()) {
				doc.add(new DoublePoint("stars", Double.parseDouble(dataset.get(i).get(14))));
				StoredField sf = new StoredField("stars", Double.parseDouble(dataset.get(i).get(14)));
				doc.add(sf);
			}
			documents.add(doc);
		}
		return documents;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

}
