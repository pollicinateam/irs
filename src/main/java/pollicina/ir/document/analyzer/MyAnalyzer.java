package pollicina.ir.document.analyzer;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.core.LetterTokenizer;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.tartarus.snowball.ext.ItalianStemmer;

import pollicina.ir.io.StopWordsManager;

public class MyAnalyzer extends Analyzer {

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		LetterTokenizer tokenizer = new LetterTokenizer();
		LowerCaseFilter lower = new LowerCaseFilter(tokenizer);
		StopFilter stop = null;
		try {
			stop = new StopFilter(lower, StopWordsManager.getAllStops());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		ItalianStemmer itStemmer = new ItalianStemmer();
		SnowballFilter stemmer = new SnowballFilter(stop, itStemmer);
		return new TokenStreamComponents(tokenizer, stemmer);
	}

}
