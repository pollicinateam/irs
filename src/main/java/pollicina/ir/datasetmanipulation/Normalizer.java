package pollicina.ir.datasetmanipulation;

public class Normalizer {
	
	private double max;
	private double min;
	private String field;

	public Normalizer(double max, double min, String field) {
		super();
		this.max = max;
		this.min = min;
		this.field = field;
	}
	
	public Double normalize(double value) {
		double normalizedValue = (value - min) / (max - min);
		return normalizedValue;
	}
	
	public Double unNormalize(double value) {
		double unNormalizedValue = (value * (max - min)) + min;
		return unNormalizedValue;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

}
