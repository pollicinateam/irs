package pollicina.ir.datasetmanipulation;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.index.IndexableField;

public class DatasetNormalizer {
	
	/**
	 * This method takes as input all documents and create normalizers for each field
	 * @param docs
	 */
	public static void normalizeNumericFields(ArrayList<Document> docs){
		for(int i = 0; i < docs.size(); i++) {
			Document doc = docs.get(i);
			List<IndexableField> fields = new ArrayList<IndexableField>(doc.getFields());
			for(int j = 0; j < fields.size(); j++) {
				String fieldName = fields.get(j).name();
				if(doc.getField(fieldName).numericValue() != null) {
					Normalizer normalizer = NormalizerFactory.createNormalizer(fieldName, docs);
//					System.out.println("max: " + normalizer.getMax());
//					System.out.println("min: " + normalizer.getMin());
					Double fieldValue = doc.getField(fieldName).numericValue().doubleValue();
					Double newFieldValue = normalizer.normalize(fieldValue);
					doc.removeField(fieldName);
					doc.add(new DoublePoint(fieldName, newFieldValue));
					StoredField sf = new StoredField(fieldName, newFieldValue);
					doc.add(sf);
				}
			}
		}
	}

}
