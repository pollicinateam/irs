package pollicina.ir.datasetmanipulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;

public class NormalizerFactory {

	private static Map<String, Normalizer> store;

	public static synchronized Normalizer getNormalizer(String field) {
		if (store == null) {
			store = new HashMap<String, Normalizer>();
		}
		if (store.containsKey(field)) {
			return store.get(field);
		}
		throw new IllegalArgumentException("Normalizer for field \'" + field + "\' has not been constructed");
	}

	public static synchronized Normalizer createNormalizer(String field, ArrayList<Document> docs) {
		if(store == null) {
			store = new HashMap<String, Normalizer>();
		}
		if(store.containsKey(field)) {
			return store.get(field);
		}
		HashMap<String, MinMax> stats = new HashMap<String, MinMax>();
		for (int i = 0; i < docs.size(); i++) {
			Document doc = docs.get(i);
			for (IndexableField indexableField : doc.getFields()) {
				if (indexableField.numericValue() != null) {
					String fieldName = indexableField.name();
					if (!stats.containsKey(fieldName)) {
						stats.put(fieldName, new MinMax());
					}
					Double fieldValue = indexableField.numericValue().doubleValue();
					stats.get(fieldName).update(fieldValue);
				}
				else if(indexableField.name().equals(field)) {
					throw new IllegalArgumentException("the field \'" + field + "\' is not numeric!");
				}
			}
		}
		for(String fieldName : stats.keySet()) {
			MinMax stat = stats.get(fieldName);
			Normalizer normalizer = new Normalizer(stat.getMax(), stat.getMin(), fieldName);
			store.put(fieldName, normalizer);
		}
		return store.get(field);
	}

}
