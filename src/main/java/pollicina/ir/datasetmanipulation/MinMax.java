package pollicina.ir.datasetmanipulation;

public class MinMax {
	
	private double min;
	private double max;
	
	public MinMax() {
		this.min = Double.MAX_VALUE;
		this.max = Double.MIN_VALUE;
	}
	
	public void update(double value) {
		if(value < min) {
			this.min = value;
		}
		if(value > max) {
			this.max = value;
		}
	}
	
	public double getMin() {
		return min;
	}
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
}